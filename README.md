
# GeneAtlas app

The source code is now available @ [https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/GeneAtlas](https://lipm-gitlab.toulouse.inra.fr/LIPM-BIOINFO/GeneAtlas/)

# Contact
Sebastien.Carrere@inrae.fr
